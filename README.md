# POC Cypress

Depêndecias:
- NODE

Passo a passo para executar os testes:
  
1.Acessar o diretório que deseja criar os testes e rodar os seguintes comandos:
  - `npm init -y`
  - `npm install cypress -y`

2.Acessar o diretório root do projeto (que tem a pasta Cypress), após instalar as dependências abrir o Cypress Runner:
  - `./node_modules/.bin/cypress open`

EXTRA: Reconhecer o Cucumber
  - `npm install --save-dev cypress cypress-cucumber-preprocessor`

  Adicione ao arquivo cypress/plugins/index.js o seguinte script:

  const cucumber = require('cypress-cucumber-preprocessor').default 
  module.exports = (on, config) => {
  on('file:preprocessor', cucumber())
  }

## PageObjects

Páginas:
- Logon
- Register (cadastro)
- Profile (perfil da ong)
- NewIncident (cadastro de casos)

Cada pagina, possui: 
- acoes -> index.js
- elementos -> elements.js
